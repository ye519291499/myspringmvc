package com.mytest.mail;

import java.io.File;
import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

public class MimeMailSend {

	private JavaMailSenderImpl mailSender;
	
	public void send(String subject,String text,String recipient,String filePath ,String fileName) throws MessagingException{
		try {
			MimeMessage mailMessage =  mailSender.createMimeMessage();
			MimeMessageHelper messageHelper = new MimeMessageHelper(mailMessage,true);
			messageHelper.setFrom(mailSender.getUsername());
			messageHelper.setTo(recipient);
			messageHelper.setSubject(subject);
			// true 表示启动HTML格式的邮件
			messageHelper.setText(
					"<html><head></head><body><h1>hello!!spring image html mail</h1>"
						+"<a href=http://www.baidu.com>baidu</a>"	+ "<img src=cid:image/></body></html>", true);
			FileSystemResource img = new FileSystemResource(new File(filePath));
			messageHelper.addAttachment(MimeUtility.encodeWord(fileName), img);//添加到附件
			 mailSender.send(mailMessage);
		} catch (MailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setMailSender(JavaMailSenderImpl mailSender) {
		this.mailSender = mailSender;
	}
	

}
