package com.mytest.mail;


import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

/**
 * 本类测试邮件发送Html形式
 * 
 * @author Eternity_._
 * 
 */
public class SingleMailSend {
	
	private MailSender mailSender;
	private SimpleMailMessage mailMessage;
	
	public void send(String subject,String text,String recipient){
		mailMessage.setSubject(subject);
		mailMessage.setText(text);
		mailMessage.setTo(recipient);
		mailSender.send(mailMessage);
	}
	
	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}
	public void setMailMessage(SimpleMailMessage mailMessage) {
		this.mailMessage = mailMessage;
	}


}
