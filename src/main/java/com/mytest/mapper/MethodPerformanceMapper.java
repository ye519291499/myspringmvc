package com.mytest.mapper;

import com.mytest.model.MethodPerformance;

public interface MethodPerformanceMapper {
	
	int insert(MethodPerformance methodPerformance);

}
