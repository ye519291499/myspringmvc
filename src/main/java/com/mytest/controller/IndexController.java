package com.mytest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.test.dubbo.registry.service.TestRegistryService;

@Controller
public class IndexController {
	//dubbo远程服务未开启会报错，先注释掉
//	@Autowired
	private TestRegistryService testRegistryService;
	
	@RequestMapping("/hello")
	public @ResponseBody Object index(Model model){
	     String name=testRegistryService.hello(" dubbo");
//	     System.out.println("xx=="+name);
		return name;
	}

}
