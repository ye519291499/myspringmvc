package com.mytest.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.mytest.annotation.PrintLog;
import com.mytest.aop.advice.PrintLogAdvice;
import com.mytest.model.UserInfo;
import com.mytest.poi.Book;
import com.mytest.poi.ExportExcel;
import com.mytest.service.IUserService;

@Controller
@RequestMapping("/user")
public class UserController {

	 @Autowired
	 private ExportExcel<UserInfo> exportExcel;
	 
	@Autowired
	private IUserService userService;

	@RequestMapping("/showInfo/{userId}")
	@PrintLog
	public String showUserInfo(ModelMap modelMap, @PathVariable int userId) {
		UserInfo userInfo = userService.getUserById(userId);
		modelMap.addAttribute("userInfo", userInfo);
		return "/showInfo";
	}

	@RequestMapping("/showInfos")
	public @ResponseBody Object showUserInfos() {
		List<UserInfo> userInfos = userService.getUsers();
		return userInfos;
	}
	
	@RequestMapping("/showInfosByPage")
	public @ResponseBody Object showUserInfoByPage(int pageNumber,  
            int pageSize) {
		  
		List<UserInfo> userInfos = userService.getUsersByPage(pageNumber,pageSize);
		 return userInfos;
	}
	
	

	@RequestMapping("/gotoLogin")
	public String gotoLogin() {
		return "/Login";
	}

	@RequestMapping("/showExcel")
	public @ResponseBody Object showExcel() {
		String[] headers = { "ID", "姓名", "学号" };
		List<UserInfo> userInfos = userService.getUsers();
//		ExportExcel<UserInfo> exportExcel = new ExportExcel<UserInfo>();
		OutputStream out;
		try {
			out = new FileOutputStream("E://user.xls");
			exportExcel.exportExcel(headers, userInfos, out);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "生成excel文档成功";
	}
	@RequestMapping("/downloadExcel")
	public ResponseEntity<byte[]> download() throws IOException {
		File file =new File("E://user.xls");
		String dfileName = new String("user.xls".getBytes("gb2312"), "iso8859-1");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		headers.setContentDispositionFormData("attachment", dfileName);
		return new ResponseEntity(FileUtils.readFileToByteArray(file), headers, HttpStatus.CREATED);
	}

}