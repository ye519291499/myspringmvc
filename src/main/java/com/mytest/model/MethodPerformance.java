package com.mytest.model;

public class MethodPerformance {
	private Long id;
	private String className;
	private String methodName;
	private Long cost;
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "MethodPerformance [id=" + id + ", className=" + className + ", methodName=" + methodName + ", cost="
				+ cost + "]";
	}
	
	
	
}
