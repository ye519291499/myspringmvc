package com.mytest.model;

public class UserInfo {
	private Long id;

	private String uname;

	private Integer unumber;
	
	public UserInfo(){}


	public UserInfo(Long id, String uname, Integer unumber) {
		super();
		this.id = id;
		this.uname = uname;
		this.unumber = unumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname == null ? null : uname.trim();
	}

	public Integer getUnumber() {
		return unumber;
	}

	public void setUnumber(Integer unumber) {
		this.unumber = unumber;
	}


}
