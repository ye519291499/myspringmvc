package com.mytest.dubbo.consunmer;

import java.util.List;  
import com.mytest.dubbo.provider.DemoService;  
import org.springframework.context.support.ClassPathXmlApplicationContext;  
  
  
public class Consumer {  
  
    public static void main(String[] args) throws Exception {  
    	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(  
                new String[] {"spring-email.xml","spring-mq.xml","spring-dubbo-consumer.xml","spring-config-db.xml","spring-mvc.xml","spring.xml"});  
        context.start();  
  
        DemoService demoService = (DemoService) context.getBean("demoService"); //  
        String hello = demoService.sayHello("tom"); // ִ  
        System.out.println(hello); //   
  
        //   
        List list = demoService.getUsers();  
        if (list != null && list.size() > 0) {  
            for (int i = 0; i < list.size(); i++) {  
                System.out.println(list.get(i));  
            }  
        }  
        // System.out.println(demoService.hehe());  
        System.in.read();  
    }  
  
} 
