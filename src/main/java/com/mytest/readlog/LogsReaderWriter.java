package com.mytest.readlog;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.mytest.model.MethodPerformance;
import com.mytest.service.IMethodPerformanceService;
/**
 * 这是一个与日志读写有关的类，定义了一些通用的方法
 * @author Devon
 *
 */
@Service("logsReaderWriter")
public class LogsReaderWriter {
	
	@Autowired
	private IMethodPerformanceService methodPerformanceService;
	
	
	/**
     * 
     * @param filePath      文件路径的字符串表示形式
     * @param KeyWords      查找包含某个关键字的信息：非null为带关键字查询；null为全文显示
     * @return      当文件存在时，返回字符串；当文件不存在时，返回null
     */
	
	public void saveLog(String filePath, String KeyWords){
		List<MethodPerformance> list = new ArrayList<MethodPerformance>();
		list = readFromFile( filePath, KeyWords);
		try {
			for(MethodPerformance mpf:list){
				methodPerformanceService.save(mpf);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
    public List<MethodPerformance> readFromFile(String filePath, String KeyWords){
      	List<MethodPerformance> list = new ArrayList<MethodPerformance>();
        StringBuffer stringBuffer = null;
        File file = new File(filePath);
        if(file.exists()){
            stringBuffer = new StringBuffer();
            FileReader fileReader = null;
            BufferedReader bufferedReader = null;
            String temp = "";
            try {
                fileReader = new FileReader(file);
                bufferedReader = new BufferedReader(fileReader);
                int i=0;
                while((temp = bufferedReader.readLine()) != null){
                    if(KeyWords ==null){
                    	if(stringBuffer.length()>0){
                    		 stringBuffer.append(" - ");
                    	}
                        stringBuffer.append(temp + "\n");
                        String tempLine = stringBuffer.toString();
                        i++;
                        if(i>2){
                        	list.addAll(stringToMethodPerformance(tempLine));
                        	i=0;
                            tempLine = null;
                            stringBuffer.setLength(0);
                        }
                    }else{
                        if(temp.contains(KeyWords)){
                            stringBuffer.append(temp + "\n");
                        }
                    }
                }
                list.addAll(stringToMethodPerformance(stringBuffer.toString()));
            } catch (FileNotFoundException e) {
                //e.printStackTrace();
            } catch (IOException e) {
                //e.printStackTrace();
            }finally{
                try {
                    fileReader.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        }
        return list;
    }
     
    private static List<MethodPerformance> stringToMethodPerformance(String tempLine) {
    	List<MethodPerformance> list = new ArrayList<MethodPerformance>();
		String [] temparry = tempLine.split(" - ");
		for (String str :temparry){
			System.out.println(str);
		}
//		com.mytest.service.impl.UserServiceImpl|getUserById|11
		for (int i=0;i<temparry.length;i++){
			if(i%2==0){
				
			}else{
				String [] strs = temparry[i].split("\\|");
				MethodPerformance mpf = new MethodPerformance();
				mpf.setClassName(strs[0]);
				mpf.setMethodName(strs[1]);
				mpf.setCost(Long.valueOf(strs[2].toString().trim()));
				System.out.println(mpf);
				list.add(mpf);
			}
		}
		return list;
	}

	/**
     * 将指定字符串写入文件。如果给定的文件路径不存在，将新建文件后写入。
     * @param log       要写入文件的字符串
     * @param filePath      文件路径的字符串表示形式，目录的层次分隔可以是“/”也可以是“\\”
     * @param isAppend      true：追加到文件的末尾；false：以覆盖原文件的方式写入
     */        
      
    public static boolean writeIntoFile(String log, String filePath, boolean isAppend){
        boolean isSuccess = true;
        //如有则将"\\"转为"/",没有则不产生任何变化
        String filePathTurn = filePath.replaceAll("\\\\", "/");
        //先过滤掉文件名
        int index = filePath.lastIndexOf("/");
        String dir = filePath.substring(0, index);
        //创建除文件的路径
        File fileDir = new File(dir);
        fileDir.mkdirs();
        //再创建路径下的文件
        File file = null;
        try {
            file = new File(filePath);
            file.createNewFile();
        } catch (IOException e) {
            isSuccess = false;
            //e.printStackTrace();
        }
        //将logs写入文件
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file, isAppend);
            fileWriter.write(log);
            fileWriter.flush();
        } catch (IOException e) {
            isSuccess = false;
            //e.printStackTrace();
        } finally{
            try {
                fileWriter.close();
            } catch (IOException e) {
                //e.printStackTrace();
            }
        }
         
        return isSuccess;
    }
    /**
     * 创建文件，如果该文件已存在将不再创建（即不起任何作用）
     * @param filePath       要创建文件的路径的字符串表示形式，目录的层次分隔可以是“/”也可以是“\\”
     * @return      创建成功将返回true；创建不成功则返回false
     */
    public static boolean createNewFile(String filePath){
        boolean isSuccess = true;
        //如有则将"\\"转为"/",没有则不产生任何变化
        String filePathTurn = filePath.replaceAll("\\\\", "/");
        //先过滤掉文件名
        int index = filePathTurn.lastIndexOf("/");
        String dir = filePathTurn.substring(0, index);
        //再创建文件夹
        File fileDir = new File(dir);
        isSuccess = fileDir.mkdirs();
        //创建文件
        File file = new File(filePathTurn);
        try {
            isSuccess = file.createNewFile();
        } catch (IOException e) {
            isSuccess = false;
            //e.printStackTrace();
        }
 
        return isSuccess;
    }
    
    public static void main(String[] args) {
    	LogsReaderWriter lrw = new LogsReaderWriter();
//   lrw.readFromFile
//		("D:\\workspace\\springmvc_mybatis_demo\\log\\myspringmvc_MethodPerformanceLog", null);
    	lrw.saveLog("D:\\workspace\\springmvc_mybatis_demo\\log\\myspringmvc_MethodPerformanceLog", null);
	}
}
