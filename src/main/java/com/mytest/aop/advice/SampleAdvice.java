package com.mytest.aop.advice;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
@Component  
@Aspect
 public class SampleAdvice {
      
      @Pointcut("execution(* com.mytest.service..*.*(..))")
      public void inServiceLayer() {
      }
      
      
      @Before(value="inServiceLayer()")  
      public void before(){  
//          System.out.println("方法执行之前执行.....");  
      }  
      
      
      
      @AfterReturning(value="inServiceLayer()")  
      public void afterReturning(){  
//          System.out.println("方法执行完执行.....");  
      }  
        
      @AfterThrowing(value="inServiceLayer()")  
      public void throwss(){  
//          System.out.println("方法异常时执行.....");  
      }  
        
      @After(value="inServiceLayer()")  
      public void after(){  
//          System.out.println("方法最后执行.....");  
      }  
        
      @Around(value="inServiceLayer()")  
      public Object around(ProceedingJoinPoint pjp){  
//          System.out.println("方法环绕start.....");  
          Object o = null;  
          try {  
              o = pjp.proceed();  
          } catch (Throwable e) {  
              e.printStackTrace();  
          }  
//          System.out.println("方法环绕end.....");  
          return o;  
      }  
      
      
 }
