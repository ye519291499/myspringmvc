package com.mytest.aop.advice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
@Component  
@Aspect
public class MethodPerformanceAdvice {
	
	
	static Logger logger2 = LogManager.getLogger(MethodPerformanceAdvice.class);
	 @Pointcut("execution(* com.mytest.service..*.*(..))")
     public void timeCost() {
     }
	
	   @Around(value="timeCost()")  
	    public Object around(ProceedingJoinPoint pjp){ 
		   
	    	   Object o = null;  
	       String methodName =pjp.getSignature().getName().toString();
	       String className = pjp.getSignature().getDeclaringTypeName();
	           try {  
	        	   long timeBefore=System.currentTimeMillis(); 
	               o = pjp.proceed();  
	               long timeAfter=System.currentTimeMillis();
	               long timeCost = timeAfter-timeBefore;
	               logger2.info(className+"|"+methodName+"|"+timeCost);
	           } catch (Throwable e) {  
	               e.printStackTrace();  
	           } 
	       	return o;
	    }  

}
