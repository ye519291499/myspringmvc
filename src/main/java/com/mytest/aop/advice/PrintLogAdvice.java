package com.mytest.aop.advice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component  
@Aspect
public class PrintLogAdvice {
	
	static Logger logger = LogManager.getLogger(LogManager.ROOT_LOGGER_NAME);
	static Logger logger2 = LogManager.getLogger();
	
	 
    @Around("com.mytest.aop.aspect.PrintLogAspect.printLog()")  
    public Object around(ProceedingJoinPoint pjp){  
    	logger.info("根日志：这是通过注解aop打印出来的日志,方法运行之前");
    	logger2.info("局部日志：这是通过注解aop打印出来的日志,方法运行之前");
    	   Object o = null;  
           try {  
               o = pjp.proceed();  
           } catch (Throwable e) {  
               e.printStackTrace();  
           } 
        	logger2.info("局部日志：这是通过注解aop打印出来的日志,方法运行之后");
           logger.info("根日志：这是通过注解aop打印出来的日志,方法运行之后");
       	return o;
    }  
    

}
