package com.mytest.aop.aspect;

import org.aspectj.lang.annotation.Pointcut;

public class PrintLogAspect {
	
	@Pointcut("@annotation(com.mytest.annotation.PrintLog)")
    public void printLog() {
    }

}
