package com.mytest.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginFilter implements Filter{

	public void init(FilterConfig filterConfig) throws ServletException {
		System.out.println("初始化LoginFilter");
		
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest httpServletRequest = (HttpServletRequest) request ;
			HttpServletResponse httpServletResponse = (HttpServletResponse)response;
			HttpSession session = httpServletRequest.getSession();
			
			if(!"1".equals(request.getParameter("isLogin"))&&!httpServletRequest.getRequestURL().toString().contains("/user/gotoLogin")){
				httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/user/gotoLogin.htmls");
				return;
			}
			
				chain.doFilter(request, response); 
	}

	public void destroy() {
		System.out.println("销毁LoginFilter");
		
	}

}
