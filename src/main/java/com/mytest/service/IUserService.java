package com.mytest.service;

import java.util.List;

import com.mytest.model.UserInfo;

public interface IUserService {

	UserInfo getUserById(int id);
	
	List<UserInfo> getUsers();
	
	int insert(UserInfo userInfo);

	List<UserInfo> getUsersByPage(int pageNumber, int pageSize);

}