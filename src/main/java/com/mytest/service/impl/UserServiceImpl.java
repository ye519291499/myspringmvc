package com.mytest.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.mytest.mapper.UserInfoMapper;
import com.mytest.model.UserInfo;
import com.mytest.service.IUserService;
@Service("userService")
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserInfoMapper userInfoMapper;

	public UserInfo getUserById(int id) {
		return userInfoMapper.selectByPrimaryKey(id);
	}

	public List<UserInfo> getUsers() {
		return userInfoMapper.selectAll();
	}
	
	public List<UserInfo> getUsersByPage(int pageNumber, int pageSize){
		System.out.println(111);
		PageHelper.startPage(pageNumber,pageSize); 
		return userInfoMapper.selectAll();
	}

	public int insert(UserInfo userInfo) {
		
		int result = userInfoMapper.insert(userInfo);
		
		System.out.println(result);
		return result;
	}

}
