package com.mytest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mytest.mapper.MethodPerformanceMapper;
import com.mytest.model.MethodPerformance;
import com.mytest.service.IMethodPerformanceService;
@Service("methodPerformanceService")
public class MethodPerformanceServiceImpl implements IMethodPerformanceService{
	@Autowired
	private MethodPerformanceMapper methodPerformanceMapper;

	public int save(MethodPerformance methodPerformance) {
		int i=0;
		methodPerformanceMapper.insert(methodPerformance);
		return i;
	}

}
