package test;

import javax.annotation.Resource;
import javax.mail.MessagingException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mytest.mail.MimeMailSend;
import com.mytest.mail.SingleMailSend;

@RunWith(SpringJUnit4ClassRunner.class)   
@ContextConfiguration(locations = { "classpath:spring.xml",
"classpath:spring-mvc.xml" })
public class TestMail {

    private SingleMailSend simpleMail; 
    
    private MimeMailSend mimeMail;
    
//    @Test  
//    public void testSingleMailSend() {   
//    	simpleMail.send("111","222","519291499@qq.com");   
//    }
    
    @Test  
    public void testMimeMailSend() throws MessagingException {   
    	mimeMail.send("111","222","519291499@qq.com","C:\\Users\\Administrator\\Desktop\\单.png","单.png");  
    }
    
    @Resource
	public void setSimpleMail(SingleMailSend simpleMail) {
		this.simpleMail = simpleMail;
	}
    @Resource
	public void setMimeMail(MimeMailSend mimeMail) {
		this.mimeMail = mimeMail;
	}
  
    
    
}
