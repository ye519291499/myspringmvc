package test;

import javax.jms.Destination;   

import org.junit.Test;   
import org.junit.runner.RunWith;   
import org.springframework.beans.factory.annotation.Autowired;   
import org.springframework.beans.factory.annotation.Qualifier;   
import org.springframework.test.context.ContextConfiguration;   
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mytest.activemq.producer.ProducerService;   
    
@RunWith(SpringJUnit4ClassRunner.class)   
@ContextConfiguration(locations = { "classpath:spring.xml",
"classpath:spring-mvc.xml" })
public class ProducerConsumerTest {   
    
    @Autowired  
    private ProducerService producerService;   
//    @Autowired  
//    @Qualifier("queueDestination")   
//    private Destination destination;   
//    
//    @Autowired  
//    @Qualifier("sessionAwareQueue")   
//    private Destination sessionAwareQueue;   
    
    @Autowired  
    @Qualifier("adapterQueue")   
    private Destination adapterQueue; 
    @Autowired  
    @Qualifier("topicDestination") 
    private Destination topicDestination;
  
//    @Test  
//    public void testMessageListenerAdapter() {   
//        producerService.sendMessage(adapterQueue, "测试MessageListenerAdapter");   
//    }   
    
    @Test  
    public void testtopicListener() {   
        producerService.sendMessage(topicDestination, "测试testtopicListener");   
    }   
       
//    @Test  
//    public void testSend() {   
//        for (int i=0; i<2; i++) {   
//            producerService.sendMessage(destination, "你好，生产者！这是消息：" + (i+1));   
//        }   
//    }   
//    
    
//  @Test  
//  public void testSessionAwareMessageListener() {   
//      producerService.sendMessage(sessionAwareQueue, "测试SessionAwareMessageListener");   
//  }   
       
}  