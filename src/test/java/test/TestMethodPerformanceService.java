package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mytest.model.MethodPerformance;
import com.mytest.service.IMethodPerformanceService;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml",
"classpath:spring-mvc.xml" })
public class TestMethodPerformanceService {
	

	@Autowired
	private IMethodPerformanceService methodPerformanceService;
	@Test
	public void testInsert() {
		MethodPerformance methodPerformance = new MethodPerformance();
		
		for(int i=0;i<10;i++){
			System.out.println("methodPerformance id= "+methodPerformanceService.save(methodPerformance));
		}
			
			
	}

}
