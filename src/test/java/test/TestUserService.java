package test;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;
import com.mytest.model.UserInfo;
import com.mytest.service.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml","classpath:spring-mvc.xml"})
//@ContextConfiguration(locations = { "classpath:spring.xml"})
public class TestUserService {
	private static final Logger LOGGER = Logger
			.getLogger(TestUserService.class);


	@Autowired
	private IUserService userService;

	
//	@Test
//	public void testQueryById1() {
//		UserInfo userInfo = userService.getUserById(1);
//		LOGGER.info(JSON.toJSON(userInfo));
//	}

/*	@Test
	public void testQueryAll() {
		List<UserInfo> userInfos = userService.getUsers();
		LOGGER.info(JSON.toJSON(userInfos));
	}*/

	@Test
	public void testInsert() {
		UserInfo userInfo = new UserInfo();
		userInfo.setUname("xiaoming");
		userInfo.setUnumber(1);
		for(int i=0;i<10;i++){
			int result = userService.insert(userInfo);
			System.out.println("userInfo id= "+userInfo.getId());
		}
			
			
	}


}
