package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mytest.model.MethodPerformance;
import com.mytest.readlog.LogsReaderWriter;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml",
		"classpath:spring-mvc.xml" })
public class TestReadLog {
	
	@Autowired
	private LogsReaderWriter logsReaderWriter;
	
	@Test
	public void testReadLog() {
		logsReaderWriter.saveLog("D:\\workspace\\springmvc_mybatis_demo\\log\\myspringmvc_MethodPerformanceLog", null);			
			
	}

}
